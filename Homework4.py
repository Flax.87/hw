# Task #1
# Дано числа a і b (a < b). Виведіть суму всіх натуральних чисел від a до b (включно).

# Створення функції.
# def sum_natural_numbers(a, b):
#     total_sum = 0
#     for number in range(a, b + 1):
#         total_sum = total_sum + number
#     return total_sum
#
# # Веденн чисел.
# a = int(input("Введіть сичло 'a': "))
# b = int(input("Введіть сичло 'b': "))
#
# # Перевірка що a < b.
#
# if a < b:
#     result = sum_natural_numbers(a, b)
#     print(f"Сума всіх натуральних чисел від {a} до {b} (включно) дорівнює {result}.")
# else:
#     print(f"Число a повинно бути меншим за число b.")

def sum_natural_numbers(a, b):
    total_sum = 0
    for number in range(a, b + 1):
        total_sum += number
    return total_sum

# Введення чисел a і b
a = int(input("Введіть число a: "))
b = int(input("Введіть число b: "))

# Перевірка, що a < b
if a < b:
    result = sum_natural_numbers(a, b)
    print(f"Сума всіх натуральних чисел від {a} до {b} (включно) дорівнює {result}")
else:
    print("Число a повинно бути менше за b")

