
# #task #1
# q1 = input("Please, write something here: ")
# q2 = input("Please, write something here one more time: ")
# print(f"{q1}, {q2}")
#-----------------------------------------------------------------------
# # #task #2
# a = 2
# b = 3
# x = 4
# dobutok = a * b * x
#
# print(f"Добуток чисел {a}, {b}, {x} = {dobutok}.")
#-----------------------------------------------------------------------
# # # #task #3
# first_input = input("Input here: ")[:10]
# summ = []
#
# print(len(first_input))
#
# for i in first_input:
#     sum_val = sum(ord(c) for c in i)
#     summ.append(sum_val)
# print(summ)

#-----------------------------------------------------------------------
# # # #task #4
# f1 = input("Please, write something here (Max number 10): ")[:10]
# number_of_symbols = len(f1)
# print(f"Here if number of rows: {number_of_symbols}")
# sumascii = 0
#
# for char in f1:
#     sumascii = sumascii + ord(char)
#
# print(f"The number of ASCII: {sumascii}")
#-----------------------------------------------------------------------
# #task #5
# f2 = input("Будь ласка, введіть речення: ")
# string = f2[::-1]
# print("Текст у зворотньому порядку:", string)
#-----------------------------------------------------------------------
# #task #6
# import math
#
# r = float(input("Please, input the radius of the circle: "))
#
# # Counting
# A = math.pi * r**2
#
# print(f"Area of a circle with radius {r} = {A}.")
#-----------------------------------------------------------------------
# #task #7
# Напишіть програму, використовуючи знання про змінні типу цілих чисел, щоб підрахувати,
# за який час транспортний засіб доїде з пункту А в пункт В,
# якщо відомі такі дані: відстань від А до В = 700 км,
# швидкість автомобіля буде постійною та дорівнює 90 км/год.
# Використовуйте формулу: Час = відстань / швидкість.
# Створіть змінні length, куди запишіть відстань, velocity, куди запишіть швидкість та змінну driving_time,
# куди запишіть результат.
# Після цього використайте метод print(), щоб вивести в консоль результат.

# velocity = int(input(
#     """З якою швидкістю ви їдете?
# Введіть швидкість тут: """))
#
# length = int(input("Введіть дистанцію: "))
# driving_time = length / velocity
# driving_time_hours = int(driving_time)
# driving_time_minutes = int((driving_time - driving_time_hours) * 60)
# print(f"Водій приїде в місце призначення через {driving_time_hours}:{driving_time_minutes}.")

#-----------------------------------------------------------------------
# #task #8
# Створіть змінну name, age, в якій зберігатиметься ім('я та вік користувача, введене з клавіатури.)
# Виведіть у консоль “My name is and I am ”, але зробіть рішення таким, щоб використовувалася конкатенація рядків
# (через +) та приведення типу числа до рядка (str(...)). Тобто, щоб якщо замінити змінні name і age,
# то в методі print() нічого не потрібно було би змінювати. Наприклад,
# a = 15, print(“Value =” + str(a)) і буде “Value = 15”.)

# name = input("""What is your name?
# Text here: """)
# age = input("""How old are you?
# Text here: """)
#
# print("My name is " + " " + str(name).title() + " and I am " + str(age)+" years old.")